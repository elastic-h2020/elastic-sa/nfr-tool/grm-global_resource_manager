#!/bin/sh

bold=$(tput bold)
normal=$(tput sgr0)

if [ $# -ne 1 ]
then
    printf "ERROR: Illegal number of parameteres supplied!\nThe Docker image has the following structure -> repository/image_name:tag_name\n"
    printf "Write the new ${bold}tag_name${normal} you want to update on .yml files. For example, execute\n$0 dev2021XXXX-alpine\n"
    exit 1
fi

echo Updating version to $1 on .yml files

sed -i -e "s@image: \"bscdataclay\/initializer:.*@image: \"bscdataclay\/initializer:$1\"@" *.yml

sed -i -e "s@image: \"bscdataclay\/logicmodule:.*@image: \"bscdataclay\/logicmodule:$1\"@" *.yml

sed -i -e "s@image: \"bscdataclay\/dsjava:.*@image: \"bscdataclay\/dsjava:$1\"@" *.yml

sed -i -e "s@image: \"bscdataclay\/dspython:.*@image: \"bscdataclay\/dspython:$1\"@" *.yml

echo WARNING: Check Dockerfile manually!! The first line should be \"FROM bscdataclay/client:$1\"

