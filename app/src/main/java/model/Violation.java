/*
*  Copyright 2020 Instituto Superior de Engenharia do Porto
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*
*/

package model;

public class Violation {
    private String dimension;
    private String nodeIP;
    private String workerIP;
    private int workerID;
    //private String COMPSsAppUuid;
    private float weight;

    public Violation(){}

    public Violation(String dimension, String nodeIP, String workerIP, int workerID, /*String COMPSsAppUuid,*/ float weight) {
        this.dimension = dimension;
        this.nodeIP = nodeIP;
        this.workerIP = workerIP;
        this.workerID = workerID;
        //this.COMPSsAppUuid = COMPSsAppUuid;
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Violation in dimension " + dimension
        + "\n\tNode IP: " + nodeIP 
        + "\n\tWorker IP: " + workerIP 
        + "\n\tWorker ID: " + workerID 
        //+ " \n\t COMPSsAppUui: "+ COMPSsAppUuid
        + "\n\tWeight: " + weight 
        +"\n";
    }

    public String getDimension() {
        return dimension;
    }

    public void setDimension(String dimension) {
        this.dimension = dimension;
    }

    public String getNodeIP() {
        return nodeIP;
    }

    public void setNodeIP(String nodeIP) {
        this.nodeIP = nodeIP;
    }
 
    public String getWorkerIP() {
        return workerIP;
    }

    public void setWorkerIP(String workerIP) {
        this.workerIP = workerIP;
    }


    public int getWorkerID() {
        return workerID;
    }

    public void setWorkerID(int workerID) {
        this.workerID = workerID;
    }


    /*public String getCOMPSsAppUuid() {
        return COMPSsAppUuid;
    }

    public void setCOMPSsAppUuid(String COMPSsAppUuid) {
        this.COMPSsAppUuid = COMPSsAppUuid;
    }*/

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

}
