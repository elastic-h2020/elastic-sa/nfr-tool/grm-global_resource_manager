/*
*  Copyright 2020 Instituto Superior de Engenharia do Porto
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*
*/

package model;

import java.util.LinkedList;

public class ViolationQueue {

    private LinkedList<Violation> queue;

    public ViolationQueue() {
        queue = new LinkedList<Violation>();
    }

    public int size() {
        return this.queue.size();
    }

    public synchronized void addViolation(Violation violation) {
        queue.addLast(violation);
        notifyAll();
    }

    public synchronized LinkedList<Violation> getViolationQueue() {
        if (queue.isEmpty())
            return null;
        return queue;
    }

    public synchronized Violation getViolationPooling() {
        if (queue.isEmpty())
            return null;

        Violation remove = queue.remove();
        return remove;
    }

    public synchronized Violation getViolation() {
        Violation remove = queue.remove();
        return remove;
    }

    public synchronized void checkViolations() {
        while (queue.isEmpty()) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    // LMP remove to process all violations

    public synchronized LinkedList<Violation> getAndRemoveListOfViolations() {
        LinkedList<Violation> list = new LinkedList<Violation>();
        while (!queue.isEmpty()) {
            list.add(queue.remove());
        }

        return list;
    }

}
