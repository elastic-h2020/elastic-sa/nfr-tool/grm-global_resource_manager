package utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class ErrorStream extends Thread {

    private InputStream is;
    //private String type;
    
    private static final Logger logger = LogManager.getLogger("ErrorStream");

    public ErrorStream(InputStream is/*, String type*/) {
        this.is = is;
        //this.type = type;
    }

    @Override
    public void run() {
        try {
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line = null;
            while ((line = br.readLine()) != null)
                logger.debug(line);
                //System.out.println(type + ">" + line);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
}
