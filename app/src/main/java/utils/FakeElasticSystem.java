/*
*  Copyright 2020 Instituto Superior de Engenharia do Porto
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*
*/

package utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.json.JSONArray;
import org.json.JSONObject;

import es.bsc.compss.nfr.model.COMPSsApplication;
import es.bsc.compss.nfr.model.CommunicationLink;
import es.bsc.compss.nfr.model.ElasticSystem;
import es.bsc.compss.nfr.model.Master;
import es.bsc.compss.nfr.model.Node;
import es.bsc.compss.nfr.model.Worker;
import es.bsc.dataclay.api.BackendID;
import es.bsc.dataclay.api.DataClay;
import es.bsc.dataclay.exceptions.metadataservice.ObjectNotRegisteredException;

public class FakeElasticSystem {

    private static float DEFAULT_CPU_THRESHOLD = 0.8f;
    private static float DEFAULT_ENERGY_THRESHOLD = 30; // W
    private static int DEFAULT_NUM_CORES = 2;
    private static float DEFAULT_CONSUMPTION_PARAMETERS = 0.0f;
    private static int DEFAULT_PID = 123;

    /**
     * Creates fake ElasticSystem
     * 
     * @param elasticSystemAlias alias of the ElasticSystem
     * @return ElasticSystem created
     * @throws Exception
     */
    public static ElasticSystem createNewElasticSystem(String elasticSystemAlias) throws Exception {
        ElasticSystem system = new ElasticSystem();
        system = initValues(system);
        system.makePersistent(elasticSystemAlias);
        return system;
    }

    /**
     * Recreates the fake ElasticSystem
     * 
     * @return updated ElasticSystem already created
     */
    public static ElasticSystem recreateNewElasticSystem(ElasticSystem elasticSystem) throws Exception {
        return initValues(elasticSystem);
    }

    private static ElasticSystem initValues(ElasticSystem system) {
        ////////////////////////////////// COMPSsAPPs
        ////////////////////////////////// /////////////////////////////////////////////////
        // App A definition:
        String appName = "AppA";
        String uuid = "A";
        int monitoringPeriod = 1;
        String infoNature = "delaySensitive";
        COMPSsApplication appA;
        try {
            appA = COMPSsApplication.getByAlias(appName);
            appA.removeWorkers();
        } catch (ObjectNotRegisteredException e) {
            appA = new COMPSsApplication();
            system.addApplication(appA);
            appA.makePersistent(appName);
        }
        appA.initValues(appName, uuid, monitoringPeriod, infoNature);

        // App B definition:
        appName = "AppB";
        uuid = "B";
        infoNature = "TCPservices";
        COMPSsApplication appB;
        try {
            appB = COMPSsApplication.getByAlias(appName);
            appB.removeWorkers();
        } catch (ObjectNotRegisteredException e) {
            appB = new COMPSsApplication();
            system.addApplication(appB);
            appB.makePersistent(appName);
        }
        appB.initValues(appName, uuid, monitoringPeriod, infoNature);

        ////////////////////////////////// NODES
        ////////////////////////////////// /////////////////////////////////////////////////////
        // Node 1 definition:
        String nodeName = "xavier-rit";
        String ipWifi = "192.168.0.68";
        String ipEth = "192.168.60.68"; // TODO: check value
        String ipLte = "192.168.0.68";
        float cpuThreshold = 0.35f;
        float energyThreshold = 20.0f;
        float signalWifi = (float) 0.0;
        int numCores = 8;
        Node node1;
        try {
            node1 = Node.getByAlias(nodeName);
        } catch (ObjectNotRegisteredException e) {
            node1 = new Node();
            system.addNode(node1);
            node1.makePersistent(nodeName);
        }
        node1.initValues(ipWifi, ipEth, ipLte, cpuThreshold, energyThreshold, signalWifi, numCores);

        // Node 2 definition:
        nodeName = "xavier-ric";
        ipWifi = "192.168.0.65";
        ipEth = "192.168.60.65"; // TODO: check value
        ipLte = "192.168.0.65";
        signalWifi = -30;
        numCores = 8;
        Node node2;
        try {
            node2 = Node.getByAlias(nodeName);
        } catch (ObjectNotRegisteredException e) {
            node2 = new Node();
            system.addNode(node2);
            node2.makePersistent(nodeName);
        }
        node2.initValues(ipWifi, ipEth, ipLte, cpuThreshold, energyThreshold, signalWifi, numCores);

        // Node 3 definition:
        nodeName = "linux";
        ipWifi = "192.168.0.27";
        ipEth = "192.168.60.27"; // TODO: check value
        ipLte = "192.168.0.27";
        cpuThreshold = 0;
        energyThreshold = 0;
        signalWifi = -30;
        numCores = 4;
        Node node3;
        try {
            node3 = Node.getByAlias(nodeName);
        } catch (ObjectNotRegisteredException e) {
            node3 = new Node();
            system.addNode(node3);
            node3.makePersistent(nodeName);
        }
        node3.initValues(ipWifi, ipEth, ipLte, cpuThreshold, energyThreshold, signalWifi, numCores);

        ////////////////////////////////// MASTERS
        ////////////////////////////////// /////////////////////////////////////////////////////
        // Master 1 definition:
        String masterName = "master1";
        Master master1;
        try {
            master1 = Master.getByAlias(masterName);
        } catch (ObjectNotRegisteredException e) {
            master1 = new Master();
            master1.setApplication(appA);
            // Set master of application
            appA.setMaster(master1);
            master1.makePersistent(masterName);
        }
        master1.initValues(node1, 123, ipEth);

        // Master 2 definition:
        masterName = "master2";
        Master master2;
        try {
            master2 = Master.getByAlias(masterName);
        } catch (ObjectNotRegisteredException e) {
            master2 = new Master();
            master2.setApplication(appB);
            // Set master of application
            appB.setMaster(master2);
            master2.makePersistent(masterName);
        }
        master2.initValues(node2, 456, ipEth);

        ////////////////////////////////// WORKERS
        ////////////////////////////////// ////////////////////////////////////////////////////
        // Add STATIC Workers
        // Worker 1A definition:
        String workerName = "worker-1A";
        final int pid1A = 32145;
        final boolean active1A = true;
        final float cpuUsage1A = 0;
        final float energyUsage1A = 0;
        final int computingUnits1A = node1.getNumCores();
        final float communicationCost1A = 0;
        Worker worker1A;
        try {
            worker1A = Worker.getByAlias(workerName);
            System.out.println("Found");
        } catch (ObjectNotRegisteredException e) {
            worker1A = new Worker();
            appA.addWorker(worker1A);
            worker1A.makePersistent(workerName);
        }
        worker1A.initValues(node1, pid1A, active1A, appA, cpuUsage1A, energyUsage1A, computingUnits1A, node1.getIpEth(),
                communicationCost1A, new ArrayList<>());

        // Worker 1B definition:
        workerName = "worker-1B";
        final int pid1B = 32146;
        final boolean active1B = true;
        final float cpuUsage1B = 0;
        final float energyUsage1B = 0;
        final int computingUnits1B = node1.getNumCores();
        final float communicationCost1B = 0;
        Worker worker1B;
        try {
            worker1B = Worker.getByAlias(workerName);
            System.out.println("Found");

        } catch (ObjectNotRegisteredException e) {
            worker1B = new Worker();
            appB.addWorker(worker1B);
            worker1B.makePersistent(workerName);
        }
        worker1B.initValues(node1, pid1B, active1B, appB, cpuUsage1B, energyUsage1B, computingUnits1B, node1.getIpEth(),
                communicationCost1B, new ArrayList<>());

        // Worker 2A definition:
        workerName = "worker-2A";
        final int pid2A = 32147;
        final boolean active2A = true;
        final float cpuUsage2A = 0;
        final float energyUsage2A = 0;
        final int computingUnits2A = node2.getNumCores();
        final float communicationCost2A = 0;
        Worker worker2A;
        try {
            worker2A = Worker.getByAlias(workerName);
            System.out.println("Found");

        } catch (ObjectNotRegisteredException e) {
            worker2A = new Worker();
            // Attach worker to COMPSsApplication
            appA.addWorker(worker2A);
            // system.addWorker(worker2A);
            worker2A.makePersistent(workerName);
        }
        worker2A.initValues(node2, pid2A, active2A, appA, cpuUsage2A, energyUsage2A, computingUnits2A, node2.getIpEth(),
                communicationCost2A, new ArrayList<>());

        // Worker 2B definition:
        workerName = "worker-2B";
        final int pid2B = 32148;
        final boolean active2B = true;
        final float cpuUsage2B = 0;
        final float energyUsage2B = 0;
        final int computingUnits2B = node2.getNumCores();
        final float communicationCost2B = 0;
        Worker worker2B;
        try {
            worker2B = Worker.getByAlias(workerName);
            System.out.println("Found");

        } catch (ObjectNotRegisteredException e) {
            worker2B = new Worker();
            // Attach worker to Node and COMPSsApplication
            // node2.addWorker(worker2B);
            appB.addWorker(worker2B);
            // system.addWorker(worker2B);
            worker2B.makePersistent(workerName);
        }
        worker2B.initValues(node2, pid2B, active2B, appB, cpuUsage2B, energyUsage2B, computingUnits2B, node2.getIpEth(),
                communicationCost2B, new ArrayList<>());

        // Worker 3A definition:
        workerName = "worker-3A";
        final int pid3A = 32149;
        final boolean active3A = true;
        final float cpuUsage3A = 0;
        final float energyUsage3A = 0;
        final int computingUnits3A = node3.getNumCores();
        final float communicationCost3A = 0;
        Worker worker3A;
        try {
            worker3A = Worker.getByAlias(workerName);
            System.out.println("Found");
        } catch (ObjectNotRegisteredException e) {
            worker3A = new Worker();
            appA.addWorker(worker3A);
            worker3A.makePersistent(workerName);
        }
        worker3A.initValues(node3, pid3A, active3A, appA, cpuUsage3A, energyUsage3A, computingUnits3A, node3.getIpEth(),
                communicationCost3A, new ArrayList<>());

        // Worker 3B definition:
        workerName = "worker-3B";
        final int pid3B = 32150;
        final boolean active3B = true;
        final float cpuUsage3B = 0;
        final float energyUsage3B = 0;
        final int computingUnits3B = node3.getNumCores();
        final float communicationCost3B = 0;
        Worker worker3B;
        try {
            worker3B = Worker.getByAlias(workerName);
            System.out.println("Found");

        } catch (ObjectNotRegisteredException e) {
            worker3B = new Worker();
            appB.addWorker(worker3B);
            worker3B.makePersistent(workerName);
        }
        worker3B.initValues(node3, pid3B, active3B, appB, cpuUsage3B, energyUsage3B, computingUnits3B, node3.getIpEth(),
                communicationCost3B, new ArrayList<>());

        ////////////////////////////////// COMMUNICATION_LINKS
        ////////////////////////////////// //////////////////////////////////
        ////////////////////////////////////////
        // CommunicationLink21 definition:
        String commuLinkName = "cl21";
        final float delayRtt = 0;
        final float plr = 0;
        final float throughput = 0;
        CommunicationLink communicationLink21;
        try {
            communicationLink21 = CommunicationLink.getByAlias(commuLinkName);
        } catch (ObjectNotRegisteredException e) {
            communicationLink21 = new CommunicationLink();
            node1.addCommunicationLink(communicationLink21);
            node2.addCommunicationLink(communicationLink21);
        }
        communicationLink21.initValues(node1, node2, node1.getIpEth(), node2.getIpEth(), delayRtt, plr, throughput);
        communicationLink21.makePersistent(commuLinkName);

        // CommunicationLink31 definition:
        commuLinkName = "cl31";
        CommunicationLink communicationLink31;
        try {
            communicationLink31 = CommunicationLink.getByAlias(commuLinkName);
        } catch (ObjectNotRegisteredException e) {
            communicationLink31 = new CommunicationLink();
            node1.addCommunicationLink(communicationLink31);
            node3.addCommunicationLink(communicationLink31);
        }
        communicationLink31.initValues(node1, node3, node1.getIpEth(), node3.getIpEth(), delayRtt, plr, throughput);
        communicationLink31.makePersistent(commuLinkName);

        // CommunicationLink32 definition:
        commuLinkName = "cl32";
        CommunicationLink communicationLink32;
        try {
            communicationLink32 = CommunicationLink.getByAlias(commuLinkName);
        } catch (ObjectNotRegisteredException e) {
            communicationLink32 = new CommunicationLink();
            node2.addCommunicationLink(communicationLink32);
            node3.addCommunicationLink(communicationLink32);
        }
        communicationLink32.initValues(node2, node3, node2.getIpEth(), node3.getIpEth(), delayRtt, plr, throughput);
        communicationLink32.makePersistent(commuLinkName);

        ///////////////////////////////////////////////////////////////////////////////////////////////

        return system;
    }

    /**
     * Creates fake ElasticSystem from a JSON file
     * 
     * @param fakeSystemPath
     * @return ElasticSystem created
     */
    public static ElasticSystem createFakeElasticSystemFromJsonFile(String fakeSystemPath) {
        String data = "";
        String resourceName = fakeSystemPath;
        try {
            File myObj = new File(resourceName);
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                data += myReader.nextLine();
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("[ERROR] Couldn't read JSON file with the FakeElasticSystem!!!");
            e.printStackTrace();
            return null;
        }

        JSONObject object = new JSONObject(data);

        String elasticSystemAlias = object.getString("ElasticSystemAlias");
        System.out.println("ElasticSystemAlias: " + elasticSystemAlias);

        Map<String, COMPSsApplication> namesAppsObjects = new HashMap<>();
        Map<String, Node> namesNodesObjects = new HashMap<>();

        ElasticSystem system = new ElasticSystem();

        System.out.println("COMPSsApplication(s): ");
        JSONArray apps = object.getJSONArray("COMPSsApplication");
        for (int i = 0; i < apps.length(); i++) {
            JSONObject appJson = apps.getJSONObject(i);
            String appName = appJson.has("appName") ? appJson.getString("appName") : "App" + i;
            String uuid = appJson.has("uuid") ? appJson.getString("uuid") : "uuid" + i;
            int monitoringPeriod = appJson.has("monitoringPeriod") ? appJson.getInt("monitoringPeriod") : 1;
            COMPSsApplication app = new COMPSsApplication(appName, uuid, monitoringPeriod);
            String infoNature = appJson.has("setInfoNature") ? appJson.getString("setInfoNature") : "";
            app.setInfoNature(infoNature);
            namesAppsObjects.put(appName, app);
            system.addApplication(app);
            System.out.println(appJson.toString());
        }

        System.out.println("Node(s): ");
        JSONArray nodes = object.getJSONArray("Node");
        for (int i = 0; i < nodes.length(); i++) {
            JSONObject nodeJson = nodes.getJSONObject(i);
            String nodeName = nodeJson.has("nodeName") ? nodeJson.getString("nodeName") : "Node" + i;
            String ipWifi = nodeJson.has("ipWifi") ? nodeJson.getString("ipWifi") : "";
            String ipEth = nodeJson.has("ipEth") ? nodeJson.getString("ipEth") : "";
            String ipLte = nodeJson.has("ipLte") ? nodeJson.getString("ipLte") : "";
            float cpuThreshold = nodeJson.has("cpuThreshold") ? nodeJson.getFloat("cpuThreshold")
                    : DEFAULT_CPU_THRESHOLD;
            float energyThreshold = nodeJson.has("energyThreshold") ? nodeJson.getFloat("energyThreshold")
                    : DEFAULT_ENERGY_THRESHOLD;
            float signalWifi = nodeJson.has("signalWifi") ? nodeJson.getFloat("signalWifi") : (float) 0.0;
            int numCores = nodeJson.has("numCores") ? nodeJson.getInt("numCores") : DEFAULT_NUM_CORES;
            Node node = new Node(ipWifi, ipEth, ipLte, cpuThreshold, energyThreshold, signalWifi, numCores);
            System.out.println("TOU2");
            system.addNode(node);
            System.out.println("TOU3");
            namesNodesObjects.put(nodeName, node);
            System.out.println("TOU4");

            BackendID machineA_BackendID = DataClay.getJavaBackend("DS1");
            System.out.println("TOU5");
            node.makePersistent(machineA_BackendID);
            System.out.println("TOU6");

            System.out.println(nodeJson.toString());
        }

        System.out.println("Communication Link(s): ");
        JSONArray cls = object.getJSONArray("CommunicationLink");
        for (int i = 0; i < cls.length(); i++) {
            JSONObject clJson = cls.getJSONObject(i);
            float delayRtt = clJson.has("delayRtt") ? clJson.getFloat("delayRtt") : DEFAULT_CONSUMPTION_PARAMETERS;
            float plr = clJson.has("plr") ? clJson.getFloat("plr") : DEFAULT_CONSUMPTION_PARAMETERS;
            float throughput = clJson.has("throughput") ? clJson.getFloat("throughput")
                    : DEFAULT_CONSUMPTION_PARAMETERS;
            String node1Str = clJson.has("node1") ? clJson.getString("node1") : "";
            String node2Str = clJson.has("node2") ? clJson.getString("node2") : "";
            if (node1Str.isEmpty() || node2Str.isEmpty()) {
                System.out.println("[WARNING] Couldn't find Nodes to create CommunicationLink!!!");
                continue;
            }
            Node node1 = namesNodesObjects.get(node1Str);
            Node node2 = namesNodesObjects.get(node2Str);
            CommunicationLink communicationLink21 = new CommunicationLink(node1, node2, node1.getIpEth(),
                    node2.getIpEth(), delayRtt, plr, throughput);
            node1.addCommunicationLink(communicationLink21);
            node2.addCommunicationLink(communicationLink21);
            System.out.println(clJson.toString());
        }

        System.out.println("Master(s): ");
        JSONArray masters = object.getJSONArray("Master");
        for (int i = 0; i < masters.length(); i++) {
            JSONObject masterJson = masters.getJSONObject(i);
            String nodeStr = masterJson.has("node") ? masterJson.getString("node") : "";
            if (nodeStr.isEmpty()) {
                System.out.println("[WARNING] Couldn't find Node to put Master!!!");
                continue;
            }
            Node node = namesNodesObjects.get(nodeStr);
            int pid = masterJson.has("pid") ? masterJson.getInt("pid") : DEFAULT_PID;
            String ip = masterJson.has("ip") ? masterJson.getString("ip") : "";
            Master master = new Master(node, pid, ip);
            String appStr = masterJson.has("setApplication") ? masterJson.getString("setApplication") : "";
            if (appStr.isEmpty()) {
                System.out.println("[WARNING] Couldn't find COMPSsApplication to set Master!!!");
                continue;
            }
            COMPSsApplication app = namesAppsObjects.get(appStr);
            master.setApplication(app);
            master.makePersistent();
            // Set master of application
            app.setMaster(master);
            System.out.println(masterJson.toString());
        }

        System.out.println("Worker(s): ");
        JSONArray workers = object.getJSONArray("Worker");
        for (int i = 0; i < workers.length(); i++) {
            JSONObject workerJson = workers.getJSONObject(i);
            String nodeStr = workerJson.has("node") ? workerJson.getString("node") : "";
            if (nodeStr.isEmpty()) {
                System.out.println("[WARNING] Couldn't find Node to put Worker!!!");
                continue;
            }
            Node node = namesNodesObjects.get(nodeStr);
            int pid = workerJson.has("pid") ? workerJson.getInt("pid") : DEFAULT_PID;
            boolean active = workerJson.has("active") ? workerJson.getBoolean("active") : true;
            String appStr = workerJson.has("addWorker") ? workerJson.getString("addWorker") : "";
            if (appStr.isEmpty()) {
                System.out.println("[WARNING] Couldn't find COMPSsApplication to add Worker!!!");
                continue;
            }
            COMPSsApplication app = namesAppsObjects.get(appStr);
            float cpuUsage = workerJson.has("cpuUsage") ? workerJson.getFloat("cpuUsage")
                    : DEFAULT_CONSUMPTION_PARAMETERS;
            float energyUsage = workerJson.has("energyUsage") ? workerJson.getFloat("energyUsage")
                    : DEFAULT_CONSUMPTION_PARAMETERS;
            float communicationCost = workerJson.has("communicationCost") ? workerJson.getFloat("communicationCost")
                    : DEFAULT_CONSUMPTION_PARAMETERS;
            int computingUnits = workerJson.has("computingUnits") ? workerJson.getInt("computingUnits")
                    : DEFAULT_NUM_CORES;
            String ip = workerJson.has("ip") ? workerJson.getString("ip") : "";
            JSONArray deactivationReasonsJsonArray = workerJson.has("deactivationReasons")
                    ? workerJson.getJSONArray("deactivationReasons")
                    : new JSONArray();
            List<String> deactivationReasons = new ArrayList<>();
            for (int j = 0; j < deactivationReasonsJsonArray.length(); j++) {
                deactivationReasons.add(deactivationReasonsJsonArray.getString(j));
            }
            Worker worker = new Worker(node, pid, active, app, cpuUsage, energyUsage, computingUnits, ip,
                    communicationCost, deactivationReasons);
            app.addWorker(worker);
            System.out.println(workerJson.toString());
        }

        system.makePersistent(elasticSystemAlias);

        return system;
    }

}
