/*
*  Copyright 2020 Instituto Superior de Engenharia do Porto
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*
*/

package utils;

import model.Violation;
import model.ViolationQueue;
import service.*;

import java.util.List;
import java.util.ListIterator;
import java.util.LinkedList;
import java.util.Map;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;

import es.bsc.compss.nfr.model.*;

public class Utils {

    private static final int STABLE_INTERVAL = 120; // seconds
    private static final int RESET_INTERVAL = 300; // seconds

    private ElasticSystem system;
    private ViolationQueue violationQueue; // TODO: this attribute is probably droped with the new version.
    private LinkedHashMap<Node, Float> bestNodes;
    private LinkedHashMap<Node, Instant> lastViolation;
    private MqttClient sender;

    private final static Logger logger = LogManager.getLogger(Utils.class.getName());

    public Utils(ElasticSystem system, ViolationQueue vq) {
        this.system = system;
        this.violationQueue = vq;
        this.bestNodes = new LinkedHashMap<>();
        this.lastViolation = new LinkedHashMap<>();
        ////////////////////////////////////// DEMO PURPOSES
        String extBroker = System.getenv("CENTRAL_MQ_URL");
        if (extBroker == null || extBroker.isEmpty()) {
            // TODO: Try to connect to another broker available on ELASTIC
            extBroker = "tcp://test.mosquitto.org";
        }
        try {
            sender = new MqttClient(extBroker, MqttClient.generateClientId());
            sender.connect();
        } catch (MqttException e) {
            System.out.println("[ERROR] Creation of MQTT Publisher failed!!!");
            e.printStackTrace();
            System.exit(1);
        }
        ////////////////////////////////////////////////////
    }

    /**
     * Get Node with greater availability
     * 
     * @return Node
     */

    // LMP: changed to include require secure nodes
    public Node getBestNode(boolean requiresSecure) {

        // Elastic system is evaluated periodically by GRM main
        // evaluateElasticSystem();
        if (bestNodes.size() < 0) {
            return null;
        }
        List<Node> list = new ArrayList<Node>(bestNodes.keySet());

        for (Node node : list) {
            if (requiresSecure == true) {
                if (node.isSecure() == true) {
                    return node;
                } else {
                    // continue searching
                }
            } else {
                return node;
            }

        }

        return null;

    }

    /**
     * Evaluate/Scan Elastic System
     * 
     * @return best Nodes ordered with free resources percentage in each Node
     */
    public void evaluateElasticSystem() {
        float weightTime = 0.5f, weightEnergy = 0.5f;
        Map<Node, Float> freedomRatioOfNode = new HashMap<>();
        system.getNodes().forEach(node -> {
            float timeThreshold = node.getCPUThreshold();
            float energyThreshold = node.getEnergyThreshold();
            List<Float> totalLoad = getTotalUsageOfNode(node);
            freedomRatioOfNode.put(node, weightTime
                    * (timeThreshold - totalLoad.get(0) + weightEnergy * (energyThreshold - totalLoad.get(1))));
        });

        bestNodes = new LinkedHashMap<>();
        freedomRatioOfNode.entrySet().stream().sorted(Map.Entry.comparingByValue())
                .forEachOrdered(x -> bestNodes.put(x.getKey(), x.getValue()));

        // LMP: update to periodically check nodes with long time witout violations

        Instant currentInstant = Instant.now();
        system.getNodes().forEach(node -> {
            
            Instant previousViolationTime = lastViolation.get(node);

            if (previousViolationTime != null)
            {
                Duration elapsedTime = Duration.between(previousViolationTime, currentInstant);
                if(elapsedTime.getSeconds() > RESET_INTERVAL) // time since last violation is greater than X seconds
                                                              // let's slowly go back to default computing units 
                {
                    for (Worker w : node.getWorkers()) {
                        if(w.isActive()) {
                            if( (w.getApplication().requiresSecure()== true && node.isSecure()) ||  w.getApplication().requiresSecure()== false){
                                if (w.getComputingUnits() < w.getMaxComputingUnits()){
                                    int diffCU = w.getComputingUnits() + 1;
                                    w.setComputingUnits(diffCU); // LMP: directly updating computing units in dataClay
                                    sendToCOMPSs("reset," + String.valueOf(diffCU) + "," + w.getNode().getIpEth());
                                }
                            }
                        }
                    }


                }
            }

        });

    }

    /**
     * Get total usage of a specific Node for Time and Energy dimension
     * 
     * @param nodeIp Node IP address
     * @return CPU and Power total usage in a specific Node
     */
    private LinkedList<Float> getTotalUsageOfNode(Node nodeToProcess /* String nodeIp */) {
        /*
         * Node nodeToProcess = null; for(Node node : system.getNodes()) {
         * if(getIpAddressOfNode(node).equals(node.getIpEth() != null ? node.getIpEth()
         * : node.getIpWifi() != null ? node.getIpWifi() : node.getIpLte() != null ?
         * node.getIpLte() : "localhost")){ nodeToProcess = node; } }
         */

        float sumTime = 0, sumEnergy = 0;
        for (Worker worker : nodeToProcess.getWorkers()) {
            if (worker.isActive()) {
                sumTime += worker.getCpuUsage();
                sumEnergy += worker.getEnergyUsage();
            }
        }

        LinkedList<Float> sumUsages = new LinkedList<>();
        sumUsages.add(sumTime);
        sumUsages.add(sumEnergy);

        return sumUsages;
    }

    /**
     * Resolve some violation with some heuristic
     * 
     * @param violation some violation in some dimension (time, energy,...)
     */

    // LMP: Commented out previous implementation of actOnViolation

    /*
     * public void actOnViolation(Violation violation) {
     * Node nodeWithViolations = getNodeByIPAddress(violation.getNodeIP());
     * if (nodeWithViolations == null) {
     * logger.warn("Node IP address {} not found!!", violation.getNodeIP());
     * return;
     * }
     * List<Worker> activeWorkers = getActiveWorkers(nodeWithViolations);
     * // If there are active workers left
     * if (activeWorkers.size() > 0) {
     * Worker w = getMinCpuUsageWorkerOfNode(nodeWithViolations);
     * if (w.getApplication().isSecure()) {
     * // TODO: Improve the decision made about a NFR violation
     * if (w.getDeadlinesMissedRatio() > 0.1) {
     * String ipAddress = Nuvla.getResourcesMetricsFromNuvla();
     * if (ipAddress == null) {
     * logger.
     * info("\tNo more suggestions.\tReason: There are no Nodes available to place Workers"
     * );
     * } else {
     * logger.info("\tMove Worker to Node with IP address {}", ipAddress);
     * }
     * } else {
     * int computingUnits = w.getComputingUnits();
     * List<String> dimensions = calculateCUDecrement(violation);
     * int computingUnitsDecrement = dimensions.size();
     * int diffCU = computingUnits - computingUnitsDecrement;
     * if (computingUnitsDecrement > 0) {
     * if (diffCU > 0) {
     * logger.info("\tWorker {} should run in {} computing units...", w.getPid(),
     * diffCU);
     * w.setComputingUnits(diffCU);
     * ////////////////////////////////////// DEMO PURPOSES
     * if (dimensions.contains("energy")) {
     * sendDemoChanges("energy," + String.valueOf(0) + "," +
     * w.getNode().getIpEth());
     * } else {
     * sendDemoChanges("0," + String.valueOf(diffCU - 1) + "," + w.getIp() + ","
     * + w.getNode().getIpEth());
     * }
     * ////////////////////////////////////////////////////
     * } else {
     * w.setActive(false);
     * w.setDeactivationReasons(dimensions);
     * logger.info("\tDeactivate Worker {}", w.getPid());
     * ////////////////////////////////////// DEMO PURPOSES
     * sendDemoChanges("deactivate," + w.getIp() + "," + w.getNode().getIpEth());
     * Node bestNode = getBestNode();
     * if (bestNode != null) {
     * try {
     * // System.out.println(bestNode.toString());
     * // System.out.println(w.getApplication().getMaster().getPid());
     * // System.out.println(w.toString());
     * sendDemoChanges("activate," + bestNode.getIpEth() + "," + w.getIp());
     * } catch (Exception e) {
     * e.printStackTrace();
     * }
     * 
     * } else {
     * logger.info(
     * "\tNo more suggestions.\tReason: There are no Nodes available to place Workers"
     * );
     * }
     * ////////////////////////////////////////////////////
     * }
     * }
     * }
     * } else {
     * logger.
     * info("\tNo more suggestions.\tReason: The COMPSsApplication is insecure.");
     * }
     * } else {
     * logger.printf(Level.INFO,
     * "\tNo more suggestions.\tReason: There are no more active Workers in Node %s. There is nothing I can do :(\n"
     * ,
     * nodeWithViolations.getIpEth());
     * }
     * 
     * }
     */

    // LMP: This version of actOnViolation supports priorityes and secure nodes
    // Also changed to process all violations currently in the queue
    // Allows to use a periodic mode
    
    public void actOnViolation(ViolationQueue violationQueue) {

        LinkedList<Violation> listOfViolations = violationQueue.getAndRemoveListOfViolations();

        if (listOfViolations.isEmpty())
            return; // allow in the future concurrent GRM objects in the same queue


        ListIterator<Violation> iterator = listOfViolations.listIterator();

        Instant currentInstant = Instant.now();

        while (iterator.hasNext()) {

            Violation violation = iterator.next();
            // This method gets the list of violated dimensions from the FIRST element
            // of the list of violations. Used to give deactivation reasons if necessary
            List<String> dimensions = nodeViolationList(violation);

            Node nodeWithViolations = getNodeByIPAddress(violation.getNodeIP());
            if (nodeWithViolations == null) {
                logger.warn("Node IP address {} not found!!", violation.getNodeIP());
                iterator = listOfViolations.listIterator(); // restart
                continue; // Continue to next node, but this is a serious error
            }
            
            // now remove and count all violations in this node
            int time_violation = 0;
            int energy_violation = 0;
            int comm_violation = 0;
            int sec_violation = 0;
            do{
                if(getNodeByIPAddress(violation.getNodeIP()) == nodeWithViolations){
                    iterator.remove();
                    String dimension = violation.getDimension();
                    if(dimension.contains("time")){ // NFR changed, "cpu" is now "time".
                        time_violation += 1;
                    } else 
                    if(dimension.contains("energy")){ // TODO: (AMB) "temperature" is also used by NFR but ignored.
                        energy_violation += 1;
                    } else 
                    if(dimension.contains("comms")){ // (AMB) "comms" is the description used, not "communication".
                        comm_violation += 1;
                    } else
                    if(dimension.contains("security")){  //  (AMB) checked and confirmed!
                        sec_violation += 1;
                    } else {
                        logger.warn("Unknown {} violation!!", dimension); // TODO: (LMP) "ram" and "temperature" are  ignored
                    }

                }
                if(iterator.hasNext()){
                    violation = iterator.next();
                } else {
                    violation = null;
                }
            }while(violation != null);

            Instant previousViolationTime = lastViolation.get(nodeWithViolations);

            if (previousViolationTime != null)
            {
                Duration elapsedTime = Duration.between(previousViolationTime, currentInstant);
                if(elapsedTime.getSeconds() < STABLE_INTERVAL) // time since last violation is less than X seconds
                                                               // let's continue to wait for stabilization
                {
                    logger.warn("Node IP address {} still not stable!!", nodeWithViolations.getNodeIP());
                    iterator = listOfViolations.listIterator(); // restart
                    continue; // Continue to next node
                }
                
            } 

            // either this is the first violation in the node, or the stable instance has already passed and we have a new violation
            // so update instant of last violation of the node 
            lastViolation.put(nodeWithViolations, currentInstant);

            if (sec_violation > 0) { // if node not secure then remove all workers
                                     // in this case all other violations are ignored, as we are changing the node load
                List<Worker> activeWorkers;

                // even if priorities are in place, if there is a non secure node, we need to remove all 
                // workers from the node that require security
                //if (system.prioritySupported() == true) {
                //    activeWorkers = getLowPrioritySecureActiveWorkers(nodeWithViolations);
                //} else {
                    activeWorkers = getSecureActiveWorkers(nodeWithViolations);
                //}

                for (Worker w : activeWorkers) {

                    w.setActive(false);
                    w.setDeactivationReasons(dimensions);
                    logger.info("\tDeactivate Worker {}", w.getPid());
                    // TODO: Check COMPSs integration
                    sendToCOMPSs("deactivate," + w.getIp() + "," + w.getNode().getIpEth());
                    Node bestNode = getBestNode(true);
                    if (bestNode != null) {
                        try {
                            // System.out.println(bestNode.toString());
                            // System.out.println(w.getApplication().getMaster().getPid());
                            // System.out.println(w.toString());
                            sendToCOMPSs("activate," + bestNode.getIpEth() + "," + w.getIp());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        
                    } else {
                        logger.info(
                                "\tNo more suggestions.\tReason: There are no Nodes available to place Workers");
                    }
                    //////////////////////////////////7

                }

            } else if (time_violation > 0 || energy_violation > 0 || comm_violation > 0) { // time and energy are processes equivalently

                List<Worker> activeWorkers;

                if (system.prioritySupported() == true) {
                    activeWorkers = getLowPriorityActiveWorkers(nodeWithViolations);
                } else {
                    activeWorkers = getActiveWorkers(nodeWithViolations);
                }

                // If there are active workers left
                if (activeWorkers.size() > 0) {
                    // For time and energy We are selecting the reduction of the worker with minimum CPU usage
                    // because eventually the impact in the reduction of computing units (and move
                    // to another node) has less impact
                    // other possibilities could be the worker with higher load to reduce the need
                    // for further changes in this node, but
                    // this could imply greater changes in other nodes
                    // But if there is a communication violation let's try first 
                    // the worker with minimum communication cost (maybe others would be better?)

                    Worker w;
                    
                    if (comm_violation > 0){
                        w = getMinCommCostWorkerofNode(activeWorkers);
                    } else {
                        w = getMinCpuUsageWorkerOfNode(activeWorkers);
                    }
                    
                    
                    // If the selected worker to have computation units reduced has deadline misses,
                    // propose to change it to another location
                    // TODO: Computing units are never incremented

                    if (w.getDeadlinesMissedRatio() > 0.0) {
                        // TODO: Why not use best node?
                        Node node = Nuvla.getResourcesMetricsFromNuvla(w.getApplication().requiresSecure(), system);
                        if (node == null) {
                            logger.info(
                                    "\tNo more suggestions.\tReason: There are no Nodes available to place Workers");
                        } else {
                            logger.info("\tMove Worker to Node with IP address {}", node.getIpEth());
                            // TODO: Check COMPSs integration
                            w.setActive(false);
                            w.setDeactivationReasons(dimensions);
                            sendToCOMPSs("activate," + node.getIpEth() + "," + w.getIp());
                            //////////////////////////////////
                        }
                    } else {
                        int computingUnits = w.getComputingUnits();

                        int computingUnitsDecrement = time_violation + energy_violation + comm_violation;

                        int diffCU = computingUnits - 1 ;// LMP, changed to just decrease one instead of  computingUnitsDecrement;

                        if (computingUnitsDecrement > 0) {
                            if (diffCU > 0) {
                                logger.info("\tWorker {} should run in {} computing units...", w.getPid(), diffCU);
                                w.setComputingUnits(diffCU);
                                // TODO: Check COMPSs integration
                                if (dimensions.contains("energy")) {
                                    sendToCOMPSs("energy," + String.valueOf(0) + "," + w.getNode().getIpEth());
                                } else {
                                    sendToCOMPSs("0," + String.valueOf(diffCU - 1) + "," + w.getIp() + ","
                                            + w.getNode().getIpEth());
                                }
                                ///////////////////////
                            } else {
                                w.setActive(false);
                                w.setDeactivationReasons(dimensions);
                                logger.info("\tDeactivate Worker {}", w.getPid());
                                // TODO: Check COMPSs integration
                                sendToCOMPSs("deactivate," + w.getIp() + "," + w.getNode().getIpEth());
                                Node bestNode = getBestNode(w.getApplication().requiresSecure());
                                if (bestNode != null) {
                                    try {
                                        // System.out.println(bestNode.toString());
                                        // System.out.println(w.getApplication().getMaster().getPid());
                                        // System.out.println(w.toString());
                                        sendToCOMPSs("activate," + bestNode.getIpEth() + "," + w.getIp());
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                } else {
                                    logger.info(
                                            "\tNo more suggestions.\tReason: There are no Nodes available to place Workers");
                                }
                                ////////////////////////////////////////////////////
                            }
                        }

                    }
                } else {
                    if (system.prioritySupported() == true) {
                        logger.printf(Level.INFO,
                                "\tNo more suggestions.\tReason: There are no more low priority active Workers in Node %s. There is nothing I can do :(\n",
                                nodeWithViolations.getIpEth());
                    } else {
                        logger.printf(Level.INFO,
                                "\tNo more suggestions.\tReason: There are no more active Workers of any priority in Node %s. There is nothing I can do :(\n",
                                nodeWithViolations.getIpEth());

                    }
                }


            }
            iterator = listOfViolations.listIterator(); // restart

        }

    }

    /**
     * Give the dimensions' list that caused violations on the same Node
     * 
     * @param violation actual violation
     * @return list of dimensions that caused violations on the same Node
     */

    // LMP: changed method name to better reflect the intent

    private List<String> nodeViolationList(Violation violation) {
        List<String> violationDimensions = new ArrayList<>();
        violationDimensions.add(violation.getDimension());

        LinkedList<Violation> violations = violationQueue.getViolationQueue();

        if (violations != null) {
            for (Violation v : violations) {
                // System.out.println(v.toString());
                if (v.getNodeIP().equals(violation.getNodeIP())
                        && !(v.getDimension().equals(violation.getDimension()))) {
                    // System.out.println(v.getDimension() + " != " + violation.getDimension());
                    violationDimensions.add(v.getDimension());
                }
            }
        }

        return violationDimensions;
    }

    /**
     * Get Node by its IP address
     * 
     * @param ipAddress IP address
     * @return Node
     */
    private Node getNodeByIPAddress(String ipAddress) {
        for (Node node : system.getNodes()) {
            List<String> ips = getIpAddressOfNode(node);
            if (ips.contains(ipAddress)) {
                return node;
            }
        }
        return null;
    }

    /**
     * Get list of IP address of specific Node
     * 
     * @param node Node in the Elastic system
     * @return IP addresses
     */
    private List<String> getIpAddressOfNode(Node node) {
        List<String> nodeIp = new ArrayList<>();
        if (node.getIpEth() != null) {
            nodeIp.add(node.getIpEth());
        }
        if (node.getIpWifi() != null) {
            nodeIp.add(node.getIpWifi());
        }
        if (node.getIpLte() != null) {
            nodeIp.add(node.getIpLte());
        }
        return nodeIp;
    }

    /**
     * Get active workers of specific Node
     * 
     * @param node Node in the Elastic system
     * @return List of active Workers
     */
    private List<Worker> getActiveWorkers(Node node) {
        List<Worker> activeWorkers = new ArrayList<>();
        for (Worker w : node.getWorkers()) {
            if (w.isActive())
                activeWorkers.add(w);
        }
        return activeWorkers;
    }

    /**
     * Get the Worker with minimum CPU usage of a specific Node
     * 
     * @param node Node in the Elastic system
     * @return Worker with minimum CPU usage
     */

    // LMP: changed to receive active workers and not node. allows reusing

    private Worker getMinCpuUsageWorkerOfNode(List<Worker> activeWorkers) {
        Worker workerWithMinCpuUsage = null;
        Float minCpuUsage = Float.NaN;

        if (activeWorkers.size() > 0) {
            for (Worker worker : activeWorkers) {
                if (minCpuUsage.isNaN() || worker.getCpuUsage() < minCpuUsage) {
                    workerWithMinCpuUsage = worker;
                    minCpuUsage = worker.getCpuUsage();
                }
            }
        } else {
            System.out.println(activeWorkers.size() + " active Workers!!");
        }
        return workerWithMinCpuUsage;
    }

    ////////////////////////////////////// DEMO PURPOSES

    /**
     * Publish changes to NFR Tools
     * 
     * @param violationMessage QoS violation committed
     */
    private void sendToCOMPSs(String violationMessage) {
        MqttMessage messageToPublish = new MqttMessage();
        try {
            messageToPublish.setPayload(violationMessage.getBytes());
            sender.publish("demo-changes", messageToPublish);
            System.out.println("[DEMO] GRM sent '" + messageToPublish + "'");
        } catch (MqttPersistenceException e) {
            e.printStackTrace();
        } catch (MqttException e) {
            e.printStackTrace();
        }

    }

    /**
     * Change Dimension's thresholds
     * 
     * @param info JSON with info to change thresholds
     */
    public void updateThreshold(String info) {
        try {
            JSONObject jo = new JSONObject(info);
            if (!jo.has("IP")) {
                throw new NullPointerException("IP is empty, I don't know what Node you want to update!!");
            }
            String ip = jo.getString("IP");
            float cpuThreshold = jo.has("cpuThreshold") ? jo.getFloat("cpuThreshold") : -1.0f;
            float energyThreshold = jo.has("energyThreshold") ? jo.getFloat("energyThreshold") : -1.0f;
            Node node = getNodeByIPAddress(ip);
            if (node == null) {
                System.out.println("[ERROR] Node IP address not found!!! I can't change the threshold/s!!!");
                return;
            }
            if (cpuThreshold != -1.0f) {
                node.setCPUThreshold(cpuThreshold);
                System.out.println("[INFO] Updated CPU threshold! Actual value = " + node.getCPUThreshold());
            }
            if (energyThreshold != -1.0f) {
                node.setEnergyThreshold(energyThreshold);
                System.out.println("[INFO] Updated ENERGY threshold! Actual value = " + node.getEnergyThreshold());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    ////////////////////////////////////////////////////////

    // LMP: added for priority support

    private List<Worker> getLowPriorityActiveWorkers(Node node) {
        List<Worker> activeWorkers = new ArrayList<>();
        for (Worker w : node.getWorkers()) {
            if (w.getApplication().isHighPriority() == false && w.isActive())
                activeWorkers.add(w);
        }
        return activeWorkers;
    }

    // LMP: added for secure support

    private List<Worker> getSecureActiveWorkers(Node node) {
        List<Worker> activeWorkers = new ArrayList<>();
        for (Worker w : node.getWorkers()) {
            if (w.isActive() && w.getApplication().requiresSecure())
                activeWorkers.add(w);
        }
        return activeWorkers;
    }

    private List<Worker> getLowPrioritySecureActiveWorkers(Node node) {
        List<Worker> activeWorkers = new ArrayList<>();
        for (Worker w : node.getWorkers()) {
            if (w.getApplication().isHighPriority() == false && w.isActive() && w.getApplication().requiresSecure())
                activeWorkers.add(w);
        }
        return activeWorkers;
    }





    // LMP Added for GRM handling communication costs

    private Worker getMinCommCostWorkerofNode(List<Worker> activeWorkers) {
        Worker workerWithMinCommCost = null;
        Float minCommCost = Float.NaN;

        if (activeWorkers.size() > 0) {
            for (Worker worker : activeWorkers) {
                if (minCommCost.isNaN() || worker.getCommunicationCost() < minCommCost) {
                    workerWithMinCommCost = worker;
                    minCommCost = worker.getCommunicationCost();
                }
            }
        } else {
            System.out.println(activeWorkers.size() + " active Workers!!");
        }
        return workerWithMinCommCost;
    }
}
