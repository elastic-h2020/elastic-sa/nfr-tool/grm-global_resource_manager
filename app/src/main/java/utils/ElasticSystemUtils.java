/*
*  Copyright 2022 Instituto Superior de Engenharia do Porto
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*
*/

package utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.json.JSONArray;
import org.json.JSONObject;

import es.bsc.compss.nfr.model.COMPSsApplication;
import es.bsc.compss.nfr.model.CommunicationLink;
import es.bsc.compss.nfr.model.ElasticSystem;
import es.bsc.compss.nfr.model.Master;
import es.bsc.compss.nfr.model.Node;
import es.bsc.compss.nfr.model.Worker;
import es.bsc.dataclay.api.BackendID;
import es.bsc.dataclay.api.DataClay;
import es.bsc.dataclay.exceptions.metadataservice.ObjectNotRegisteredException;

public class ElasticSystemUtils {

    /**
     * Prints current status for a given ELASTIC system
     * 
     * @param system Elastic system
     */
    public static void printCurrentElasticSystem(ElasticSystem system) {
        System.out.println();
        System.out.println("Deployed ELASTIC system:");
        System.out.println("Nodes:");
        system.getNodes().forEach(node -> {
            System.out.println(node.toString());
        });
        system.getApplications().forEach(app -> {
            System.out.println(String.format("App: %s", app.getName()));
            if (app.getMaster() != null && app.getMaster().getNode() != null) {
                System.out.println(String.format("\tMaster in %s with PID %d",
                        (app.getMaster().getNode().getIpWifi() == null ? app.getMaster().getNode().getIpEth()
                                : app.getMaster().getNode().getIpWifi()),
                        app.getMaster().getPid()));
            }
            System.out.println("\tWorkers:");
            app.getWorkers().forEach(w -> System.out
                    .println(String.format("\t\t- %s (PID: %d), active = %b", w.getIp(), w.getPid(), w.isActive())));

            System.out.println();
        });
    }

    /**
     * Returns list with Workers of an ELASTIC system
     * 
     * @param system  elasticSystem
     * @param localIP IP address of Node
     * @return list of Workers of a specific Node
     */
    public static List<Worker> getWorkersToMonitor(ElasticSystem system, String localIP) {
        List<Worker> workerList = new ArrayList<Worker>();

        // system.getApplications().forEach(app -> {
        system.getNodes().forEach(node -> {
            node.getWorkers().forEach(worker -> {
                // if (worker.getIp().equals(localIP)) {
                workerList.add(worker);
                // }
            });
        });
        // });

        return workerList;
    }
}
