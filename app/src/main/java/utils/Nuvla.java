/*
*  Copyright 2020 Instituto Superior de Engenharia do Porto
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*
*/

package utils;

import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import java.io.InputStreamReader;
import java.io.BufferedReader;

import org.json.JSONObject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;

import java.io.IOException;

import es.bsc.compss.nfr.model.*;

public class Nuvla {

    /***************************************************************************************/
    /******************* METHODS RELATED WITH RESOURCES METRICS FROM NUVLA *****************/
    /***************************************************************************************/

    private final static float WEIGHT_TIME = 0.3f;
    private final static float WEIGHT_ENERGY = 0.1f;
    private final static float WEIGHT_COMMUNICATION = 0.2f;
    // LMP: removed weight of secure. This is a on/off requiresSecure option
    // private final static float WEIGHT_SECURITY = 0.4f; 
    private final static Logger logger = LogManager.getLogger(Nuvla.class.getName());

    /**
     * Executes Python script that pulls all resources' metrics from Nuvla API and
     * returns the IP address of the Node with greater availability
     * 
     * @return IP address of the Node with greater availability
     */

     //LMP: changed to consider applications that require secure nodes
    public static Node getResourcesMetricsFromNuvla(boolean requiresSecure,  ElasticSystem system) {
        Node nuvlaNode = null;
        try {
            Process getNuvlaStatus = Runtime.getRuntime().exec("python3 getNuvlaStatus.py");
            JSONArray nodesResources = getAllNodesResources(getNuvlaStatus);
            nuvlaNode = nodesEvaluation(nodesResources, WEIGHT_TIME, WEIGHT_ENERGY, WEIGHT_COMMUNICATION,
                    requiresSecure, system);
        } catch (IOException exception) {
            logger.error("Pull of all resources' metrics information failed!!!");
            exception.printStackTrace();
        }
        return nuvlaNode;
    }

    /**
     * Reads result of Python script that pulls information from Nuvla API
     * 
     * @param proc process that executes Python script
     * @return JSONArray with all resources' metrics of all Elastic system Nodes
     */
    private static JSONArray getAllNodesResources(Process proc) throws IOException {
        ErrorStream errorStream = new ErrorStream(proc.getErrorStream());
        errorStream.setName("U-Nuvla");
        errorStream.start();

        BufferedReader stdInput = new BufferedReader(new InputStreamReader(proc.getInputStream()));
        String output = null;
        JSONArray ja = new JSONArray();
        while ((output = stdInput.readLine()) != null) {
            for (String s : output.split("\n")) {
                JSONObject jo = new JSONObject(s);
                ja.put(jo);
            }
        }

        return ja;
    }

// LMP: changed to consider requiresSecure and return Node

    /**
     * Evaluate the workload of the available Nodes
     * 
     * @param nodeResources       JSONArray with all resources' metrics of all
     *                            Elastic system Nodes
     * @param weightTime          percentage weight for NFR Time
     * @param weightEnergy        percentage weight for NFR Energy
     * @param weightCommunication percentage weight for NFR Communication
     * @param weightSecurity      percentage weight for NFR Security
     * @return Node based on IP address of a specific Nuvla(Node)
     */
    private static Node nodesEvaluation(JSONArray nodeResources, float weightTime, float weightEnergy,
            float weightCommunication, boolean requiresSecure, ElasticSystem system) {
        Node nodeWithGreaterAvailability = null;
        String nodeIP;
        float minAvailability = 0.0f;
        for (Object obj : nodeResources) {
            if (obj instanceof JSONObject) {
                JSONObject jo = ((JSONObject) obj);
                float timeAvail = evaluateTime(jo);
                float energyAvail = evaluateEnergy(jo);
                // System.out.println("timeAvail="+timeAvail);
                // System.out.println("currentAvail="+energyAvail);
                float nodeAvailability = timeAvail * weightTime + energyAvail * weightEnergy;
                if (nodeWithGreaterAvailability == null || minAvailability > nodeAvailability) {
                    nodeIP = jo.getString("ip");
                    Node node = system.IPtoNode(nodeIP);
                    if(node != null) {
                        if (requiresSecure){
                            if(node.isSecure()){
                                nodeWithGreaterAvailability = node;
                                minAvailability = nodeAvailability;
                            } else {
                                //nothing to do, need to continue searching
                            }
                        } else {
                            nodeWithGreaterAvailability = node;
                            minAvailability = nodeAvailability;
                        }

                    } else {
                        System.out.println("ERROR! Nuvla IP does not map to a node");
                    }
                }
            }
        }
        return nodeWithGreaterAvailability;
    }

    /**
     * Calculate the availability for the dimension time specifically
     * 
     * @param jo JSONObject with resources' metrics
     * @return availability in terms of time
     */
    private static float evaluateTime(JSONObject jo) {
        float weightCpu = 0.6f;
        float weightRam = 0.4f;
        float availability = 0.0f;
        float availabilityCpu = 0.0f;
        float availabilityRam = 0.0f;
        if (jo.has("cpu")) {
            JSONObject joCpu = jo.getJSONObject("cpu");
            if (joCpu.has("load") && joCpu.has("capacity")) {
                availabilityCpu = 1 - (joCpu.getFloat("load") / joCpu.getInt("capacity"));
            }
        }
        if (jo.has("ram")) {
            JSONObject joRam = jo.getJSONObject("ram");
            if (joRam.has("used") && joRam.has("capacity")) {
                availabilityRam = 1 - (joRam.getFloat("used") / joRam.getInt("capacity"));
            }
        }

        availability = (availabilityCpu * weightCpu) + (availabilityRam * weightRam);
        return availability;
    }

    /**
     * Calculate the availability for the dimension energy specifically
     * 
     * @param jo JSONObject with resources' metrics
     * @return availability in terms of energy
     */
    private static float evaluateEnergy(JSONObject jo) {
        float totalPowerConsumed = 0.0f;
        Map<String, Double> currentInformation = new HashMap<>();
        Map<String, Double> currentLimitInformation = new HashMap<>();
        float currentAvailability = 0.0f;
        if (jo.has("power-consumption")) {
            JSONArray ja = (JSONArray) jo.get("power-consumption");
            for (int i = 0; i < ja.length(); i++) {
                JSONObject joPower = ja.getJSONObject(i);
                if (((String) joPower.get("metric-name")).contains("_power")) {
                    totalPowerConsumed += joPower.getFloat("energy-consumption");
                }
                if (joPower.get("unit").equals("mA")) {
                    String metric_name = ((String) joPower.get("metric-name")).split("_")[0];
                    Double metric_value = (Double) joPower.get("energy-consumption");
                    if (((String) joPower.get("metric-name")).contains("_critical_current_limit")) {
                        currentLimitInformation.put(metric_name, metric_value);
                    } else {
                        currentInformation.put(metric_name, metric_value);
                    }
                }
            }
        }
        // System.out.println("totalPowerConsumed="+totalPowerConsumed);

        int count = 0;
        for (String key : currentLimitInformation.keySet()) {
            if (currentInformation.containsKey(key)) {
                currentAvailability += (1 - (currentInformation.get(key) / currentLimitInformation.get(key)));
                count++;
            }
        }
        currentAvailability /= count;
        return currentAvailability;
    }

}
