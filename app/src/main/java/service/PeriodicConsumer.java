/*
*  Copyright 2022 Instituto Superior de Engenharia do Porto
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*
*/

package service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import model.Violation;
import model.ViolationQueue;
import utils.Utils;

public class PeriodicConsumer implements Runnable {

    private ViolationQueue violationQueue;
    private Utils utils;
    private long period;

    private final Logger logger = LogManager.getLogger(SporadicConsumer.class.getName());

    public PeriodicConsumer(ViolationQueue violationQueue, Utils utils, long period) {
        this.violationQueue = violationQueue;
        this.utils = utils;
        this.period = period;
    }

    // Loop to consume violation messages sent by Time and Energy Monitors
    @Override
    public void run() {
        String threadName = Thread.currentThread().getName();
		logger.traceEntry("{} is alive!", threadName);

        while (!Thread.interrupted()) {
            try {
                Thread.sleep(period);
                //logger.info("Suggestions for COMPSs scheduler:");
                utils.actOnViolation(violationQueue);
            } catch (InterruptedException e) {
                
                e.printStackTrace();
            }
         
        }
    }

}
