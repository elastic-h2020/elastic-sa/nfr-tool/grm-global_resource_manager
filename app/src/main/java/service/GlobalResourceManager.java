/*
*  Copyright 2020 Instituto Superior de Engenharia do Porto
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*
*/

package service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;

import es.bsc.compss.nfr.model.ElasticSystem;
import es.bsc.dataclay.api.DataClay;
import es.bsc.dataclay.exceptions.metadataservice.ObjectNotRegisteredException;
import model.ViolationQueue;
import utils.ElasticSystemUtils;
import utils.FakeElasticSystem;
import utils.Utils;

public class GlobalResourceManager {

    private final static String TOPIC = "violations";
    private final static Logger logger = LogManager.getLogger(GlobalResourceManager.class.getName());
    private static Thread consumerThread;

    // LMP: added a possible periodic model to consumer thread.
    // added periodic wait for elastic system and periodic evaluate of elastic system
    private final static boolean PERIODIC_MODEL = true;
    private final static long CONSUMER_PERIOD = 60000;
    private final static long EVALUATE_PERIOD = 120000;
    private final static long WAIT_SYSTEM = 30000;

    public static void main(String[] args) throws Exception {
        int loopWaitForElasticSystemCounter = 0;
        logger.info("Started Global Resource Manager!");
        try {
            // Init dataClay session
            DataClay.init();
            logger.info("Started dataClay session!");
        } catch (Exception e) {
            // e.printStackTrace();
            logger.error(e.toString());
            logger.error("Unable to start dataclay. Aborting...");
            System.exit(1);
        }

        // FIXME: None of the solutions working. Talk with Daniel.
        // try{
        // DataClayObject.deleteAlias(ELASTIC_SYSTEM_ALIAS);
        // DataClayObject.deleteAlias("ElasticSystem", ELASTIC_SYSTEM_ALIAS);
        // ElasticSystem.deleteAlias(ELASTIC_SYSTEM_ALIAS);
        // } catch (Exception e) {
        // System.out.println("[ERROR] Failed removing fake ELASTIC system!!!");
        // e.printStackTrace();
        // }

        ElasticSystem elasticSystem = null;
        // FakeElasticSystem.createFakeElasticSystemFromJsonFile("./fake/FakeElasticSystem.json");
        boolean exist = false;
        String ELASTIC_SYSTEM_ALIAS = System.getenv("ELASTIC_SYSTEM_ALIAS");
        while (!exist && loopWaitForElasticSystemCounter < 10) {
            try {
                logger.info("Searching for ELASTIC system created...");
                elasticSystem = ElasticSystem.getByAlias(ELASTIC_SYSTEM_ALIAS);
                logger.info("ELASTIC system exists!");
                exist = true;
            } catch (ObjectNotRegisteredException e) {
                logger.info("ELASTIC system has not been deployed.");
            }
            /*
             * if (!exist) {
             * try {
             * logger.info("Creating fake ELASTIC system with alias " +
             * ELASTIC_SYSTEM_ALIAS);
             * elasticSystem =
             * FakeElasticSystem.createNewElasticSystem(ELASTIC_SYSTEM_ALIAS);
             * } catch (Exception e) {
             * logger.error("Creation of fake ELASTIC system failed!!!");
             * e.printStackTrace();
             * }
             * } else {
             * // TODO: WHAT?!??! ARE WE RECREATING A FAKE SYSTEM WHEN WE HAVE A REAL
             * SYSTEM?
             * // Shouldn't we leave it as it is?
             * elasticSystem = FakeElasticSystem.recreateNewElasticSystem(elasticSystem);
             * }
             * 
             */
            try {
                Thread.sleep(WAIT_SYSTEM);
            } catch (Exception e) {
                e.printStackTrace();
            }
            loopWaitForElasticSystemCounter += 1;
        }

        if (exist) {
            ElasticSystemUtils.printCurrentElasticSystem(elasticSystem);
            ViolationQueue violationQueue = new ViolationQueue();
            Utils utils = new Utils(elasticSystem, violationQueue);
            elasticSystem.setPrioritySupported(true); // Priorities are now on 

            // LMP: changed to add the possibility of a periodic consumer
            
            if (PERIODIC_MODEL == true) {

                PeriodicConsumer consumer = new PeriodicConsumer(violationQueue, utils, CONSUMER_PERIOD);
                consumerThread = new Thread(consumer, "Periodic NFR Violations Consumer");
            } else {

                SporadicConsumer consumer = new SporadicConsumer(violationQueue, utils);
                consumerThread = new Thread(consumer, "NFR Violations Consumer");
            }
            consumerThread.start();

            try {
                String extBroker = System.getenv("CENTRAL_MQ_URL");
                if (extBroker == null || extBroker.isEmpty()) {
                    // TODO: Try to connect to another broker available on ELASTIC
                    extBroker = "tcp://test.mosquitto.org";
                }
                MqttClient client = new MqttClient(extBroker, MqttClient.generateClientId());
                MqttConnectOptions options = new MqttConnectOptions();
                options.setCleanSession(true);
                options.setConnectionTimeout(10);
                client.setCallback(new ConsumerService(violationQueue, utils));
                client.connect(options);
                client.subscribe(TOPIC);
                logger.info("Waiting for messages of NFR violations...");
            } catch (Exception e) {
                logger.error("Failed subscribing NFR Violations!!!");
                e.printStackTrace();
            }

            while (!Thread.interrupted()) {
                try {
                    Thread.sleep(EVALUATE_PERIOD);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                utils.evaluateElasticSystem();
                logger.info("Global Resource manager evaluated the ElasticSystem!");
            }

            consumerThread.join();

        }

        // ELASTIC System does not exist after trying for a while
        // try to exit gracefully

        try {
            exitGracefully();
        } catch (Exception e) {
            logger.error(e.toString());
            logger.error("Unable to stop dataclay. Aborting...");
            System.exit(1);
        }

    }

    private static void exitGracefully() throws Exception {
        DataClay.finish();
        System.exit(0);
    }
}
