/*
*  Copyright 2020 Instituto Superior de Engenharia do Porto
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*
*/

package service;

import com.google.gson.Gson;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import model.Violation;
import model.ViolationQueue;
import utils.Utils;

public class ConsumerService implements MqttCallback {

    private Gson converter;
    private ViolationQueue violationQueue;
    private Utils utils;

    private final Logger logger = LogManager.getLogger(ConsumerService.class.getName());

    public ConsumerService(ViolationQueue violationQueue, Utils utils) {
        this.converter = new Gson();
        this.violationQueue = violationQueue;
        this.utils = utils;
    }

    public void connectionLost(Throwable cause) {
        logger.fatal("Connection to MQTT broker lost!"
                // + " with cause \"" + cause.getMessage() + "\""
                // + " Reason code " + ((MqttException)cause).getReasonCode()
                // + "\n Cause -> " + ((MqttException) cause).getCause()
                );
        cause.printStackTrace();
        // TODO: Reconnect
    }

    public void messageArrived(String s, MqttMessage mqttMessage) throws Exception {
        String messageArrived = new String(mqttMessage.getPayload());
        ////////////////////////////////////// DEMO PURPOSES
        if (messageArrived.contains("Threshold")) {
            logger.info("Received threshold update {}", messageArrived);
            utils.updateThreshold(messageArrived);
        } else {
            ////////////////////////////////////////////////////
            logger.info("Received violation {}", messageArrived);
            Violation violation = converter.fromJson(messageArrived, Violation.class);
            violationQueue.addViolation(violation);
        }
    }

    public void deliveryComplete(IMqttDeliveryToken token) {
        try {
            logger.info("Delivery token \"{}\" received.", token.hashCode());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
