#!/usr/bin/env python

import os
# DOCUMENTATION: https://github.com/nuvla/python-library
from nuvla.api import Api as nuvla_Api
import json
import datetime

nuvla_api = nuvla_Api(os.environ['NUVLA_ENDPOINT'], insecure=True)
#nuvla_api = nuvla_Api(NUVLA_ENDPOINT, insecure=True)
nuvla_api.login_apikey(os.environ['NUVLA_API_KEY'], os.environ['NUVLA_API_SECRET'])
#nuvla_api.login_apikey(NUVLA_API_KEY, NUVLA_API_SECRET)

response = nuvla_api.search('nuvlabox-status')
for i in range(response.count):
    #print(response.resources[i])
    last_updated_str = response.resources[i].data.get("updated")
    date_time_obj = datetime.datetime.strptime(last_updated_str, '%Y-%m-%dT%H:%M:%S.%fZ')
    current_time = datetime.datetime.now()
    diff_time = (current_time - date_time_obj).total_seconds() / 60
    # If Nuvla was updated in the last 5 minutes
    if  diff_time < 5:
        resources = response.resources[i].data.get("resources")
        resources["nuvlabox-status"] = str(response.resources[i].data.get("id")).split("/")[1]
        resources["hostname"] = str(response.resources[i].data.get("hostname"))
        resources["ip"] = str(response.resources[i].data.get("ip"))
        resourcesJson = json.dumps(resources)
        print(resourcesJson)
   
