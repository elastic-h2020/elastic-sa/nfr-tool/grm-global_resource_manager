#!/bin/bash

cd ../dataclay/stubs
jar cvf ../stubs.jar .
mvn install:install-file -Dfile=../stubs.jar -DgroupId=es.bsc.compss -DartifactId=nfrtool-dataclay-stubs -Dversion=2.0 -Dpackaging=jar -DcreateChecksum=true
