# README #

This demonstration shows the Global Resource Manager tool accomplishing several tasks, such as:  
- listen to various NFR tools in case of NFR violations  
- make decisions about NFR violations to optimize the system workload  
- communicate with dataClay for COMPSs to be able to read the proposed decisions  
  

**Important: the dataclay folder is a Git submodule and must be initialized by calling**
```
git submodule update --init --recursive
```
**IMMEDIATELY AFTER cloning this repository (first time) otherwise** 
```
git submodule update --remote --recursive
```

# Structure
- The **globalrm** folder contains Global Resource Manager tool  
- The **dataclay** folder should correspond to the GitLab repo https://gitlab.bsc.es/elastic-h2020/elastic-sa/nfr-tool/dataclay  


# SETUP  
There are two options:
## 1. In the device/machine
To use this demo, a connection to dataClay is mandatory. Before establishing this connection, run the **setupForDataclay.sh** script to configure some necessary settings.  
You can check usage by executing:  
```
./setupForDataclay.sh -h
```
For example, if the machine/Node has dataClay's logicModule, execute "./setupForDataclay.sh LM NFRtoolUser"  

Start the dataClay, destroying any dataClay instance that could be active.  
```
docker-compose -f master-dataclay.yml -f backend-dataclay.yml down -v --remove-orphans
```

If this Node is the **"master"** node, run:  
``` 
docker-compose -f master-dataclay.yml up --build   
```
If this Node is one of the **backend** nodes, execute:  
``` 
docker-compose -f backend-dataclay.yml up --build   
```

**NOTE 1:** In release 2.5, dcinitializer should only start when all dataClay nodes are up! You can use development versions to add nodes "on demand".

Since this demo uses the Nuvla API, you need to obtain your credentials. 
To create an API key (credentials): 
1. Go to https://nuvla.io/ui/api/credential 
2. Search...
3. \+ add 
4. generate-api-key 
5. create
Save the secret key (look for a notification at the top righ corner)

**NOTE 2:** API key is the id after credential/

In .env.example replace "XXX ..." with your KEY and SECRET, and rename the file by removing ".example".


After **dcinitializer** or **logicmodule** services inform that the data model is registered in some namespace (in this case, ElasticNFR), start the Global Resource Manager (GRM) and the NFR Tools on the different Nodes,  executing:  
``` 
docker-compose down -v  
docker-compose up --build   
```

## 2. Through Nuvla 
Log in to Nuvla.
In the left bar, select Apps, and launch the **dataClay standalone** application. 
1. Select the infrastructure service you want to deploy dataClay (see the **NOTE 3**)
2. Modify the Environment variables needed:

Environment variables | Value
--- | --- 
| HOSTNAME | < IP address of the device > |  
| USER | NFRtoolUser |  
| GIT_JAVA_MODELS_URLS | https://gitlab.bsc.es/elastic-h2020/elastic-sa/nfr-tool/dataclay |
| GIT_JAVA_MODELS_PATHS | model |
| JAVA_NAMESPACES | ElasticNFR |

3. Accept the license agreement
4. Launch

In the left bar, select Apps, and launch the **GRM** application.
1. Select the infrastructure service you want to deploy GRM (see the **NOTE 3**)
2. Modify the Environment variables needed:

Environment variables | Value
--- | --- 
| HOSTNAME | < IP address of the device with dataClay logic module> |  

3. Launch


**NOTE 3:** If you don't have an infrastructure service, watch this video https://youtu.be/XSBDLLr-qSk?list=PLY20wTTKSDHa-bTvE-hRtAXafrNNkQYru

# Warnings
### Clean volumes from host directory
If in GRM deployment the exception 
```
es.bsc.dataclay.exceptions.metadataservice.AliasAlreadyInUseException
```
**OR** an error like
```
[ERROR] Failed to execute goal org.apache.maven.plugins:maven-compiler-plugin:3.1:compile (default-compile) on project global.rm: Compilation failure: Compilation failure: 
[ERROR] /demo/src/main/java/model/GlobalResourceManager.java:[28,13] cannot find symbol
[ERROR]   symbol:   class ElasticSystem
...
[ERROR]   symbol:   class Node
...
```
is raised, remove all files of the parent directory where the model is located and registered. You can check this path by looking at **master-dataclay.yml**, in the **dcinitializer** service, then **volumes**, and search for a line like
```
- \< PATH TO DATACLAY MODEL WITH ELASTICSYSTEM WHERE POM.XML IS LOCATED \>:/model/:rw 
```
For example, in this repository, you should remove every file from **/opt/dataclay/** and then, restart dataClay.  
Also ensure that **dcinitializer** exited with code 0 OR, at least, printed a message similar to 
```
[dataClay] Data model registered in namespace: ElasticNFR
```

### Check dependencies
If you followed the second approach, the **dataClay standalone** application is using the current development version of dataClay (allowing the registration of models in Git), so check (in your pom.xml files) if your applications are using 2.6.1-SNAPSHOT dependency of dataClay, i.e.
```
<dependency>
	<groupId>es.bsc.dataclay</groupId>
        <artifactId>dataclay</artifactId>
        <version>2.6.1-SNAPSHOT</version>
</dependency>
<repositories>
        <repository>
                <id>oss.sonatype.org-snapshot</id>
                <url>http://oss.sonatype.org/content/repositories/snapshots</url>
                <snapshots>
                        <enabled>true</enabled>
                </snapshots>
        </repository>
</repositories>
```
### Check accounts
If in GRM deployment the error 
```
[dataClay] ERROR Invalid account
```
is raised, check that the user accounts are named equally in the configuration and .yml files (-USER=${USER:-defaultUser}). If they are not, put it in as well. 


# Demo behavior
The Global Resource Manager creates a fake Elastic System with an alias equals to "system".
Then, it listens to NFRViolations in the "violations" queue name (topic) and advises some actions, e.g. reducing computing units for a specific Worker or deactivate a Worker.


# Acknowledgements
This work has been supported by the EU H2020 project ELASTIC, contract #825473.

