#!/bin/bash

echo "Removing old dockers and cleaning environment"
docker-compose -f master-dataclay.yml -f backend-dataclay.yml kill
docker-compose -f master-dataclay.yml -f backend-dataclay.yml down -v --remove-orphans
docker-compose down -v
sudo rm -rf /opt/dataclay/*
mkdir -p /opt/dataclay

dataclay_yml="master-dataclay.yml"
echo "PARAMS: $#"
if [ $# -eq 1 ];
then
	dataclay_yml=$1
fi

echo "Deploying dataclay dockers"
docker-compose -f $dataclay_yml up --build -d

# check if dockers already up and registered
res=$(docker-compose -f $dataclay_yml logs 2>&1 | grep "+ CONTRACTID=")
while [ -z "$res" ];
do
        echo "Dataclay initializer not ready yet. Waiting for it to finish..."
        sleep 3
	res=$(docker-compose -f $dataclay_yml logs 2>&1 | grep "+ CONTRACTID=")
done
echo "Dataclay initializer registered model, stubs can be retrieved."
sleep 20

# to run the simulator, debug, or whatever
./GetStubs.sh
jar cvf stubs.jar -C stubs .
mvn install:install-file -Dfile=stubs.jar -DgroupId=es.bsc.compss \
		         -DartifactId=nfrtool-dataclay-stubs      \
			 -Dversion=1.0 -Dpackaging=jar            \
			 -DcreateChecksum=true


docker-compose up --build -d
